﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_SOLID
{
    public interface IGameResults
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
