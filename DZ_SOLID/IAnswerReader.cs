﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_SOLID
{
    public interface IAnswerReader
    {
        int Read(int tryNumber);
    }
}
