﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_SOLID
{
    public interface IGameToPlay
    {
        IGameResults Play(ISettingsGetter settingsGetter, IAnswerReader answerReader);
    }
}
