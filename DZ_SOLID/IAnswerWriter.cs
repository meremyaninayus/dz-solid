﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_SOLID
{
    public interface IAnswerWriter
    {
        public void Write(IGameResults gameResults);
    }
}
