﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_SOLID
{
    public class GameResults:IGameResults
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
